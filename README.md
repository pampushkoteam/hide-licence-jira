## Project overview

Плагин скрывает из веб-интерфейса JIRA элементы сообщающие о статусе лицензии.
  
При этом для системных администраторов интерфейс остаётся без изменений.
  
## Contributors

Alexander Pampushko
   
## License
  
Apache License 2.0