package pmpshk.jira.ui.conditions;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;

/**
 *
 */
public class HideLicenceCondition extends AbstractWebCondition
{
	@Override
	public boolean shouldDisplay(ApplicationUser apUs, JiraHelper jiraHelper)
	{
		//todo : параметры метода почему-то равны null - разобраться почему
		
		boolean hideLicence = true; //по умолчанию скрываем лицензию для всех
		
		JiraAuthenticationContext jiraAuthenticationContext = ComponentAccessor.getJiraAuthenticationContext();
		if (jiraAuthenticationContext.isLoggedInUser())
		{
			ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
			if (user != null)
			{
				
				boolean isTargetUser = ComponentAccessor.getGroupManager().isUserInGroup(user, "AWG-LM");
				//если это юзер из указанной группы
				if (isTargetUser)
				{
					hideLicence = false; //то лицензию не скрываем
				}
			}
		}
		return hideLicence;
	}
}
